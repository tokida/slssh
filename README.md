SLSSH
====

OverView

SoftLayer上のサーバに対してSSHを利用してログインをする.

## Description

SoftLayerではLinuxであれば初期にRootのパスワードが自動的に生成される。このスクリプトは自動的で初期設定されているrootパスワードを利用してログインする事が出来ます。このことにより作業の簡略化を行うことが出来ます。

## Requirement

* sl ( need python )
* sshpass

## Usage

```
slssh [Options] hostname

Options:
  -h, --help 
       --version
  -p, --private :  プライベートアドレスに対してログインする

```

## Licence

Copyright (c) 2015 Tokida tokihide@gmail.com

This software is released under the MIT License, see LICENSE.txt.